//
//  ContentView.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 19/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
