//
//  CurrentView.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 27/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import SwiftUI

struct CurrentView: View {
    
    @State private var name:String  = "Loading"
    
    
    private let viewModel: CurrentViewModel
    
    init(viewModel: CurrentViewModel) {
      self.viewModel = viewModel
    }
    
    
    var body:some View {
        HStack(alignment: .top){
            VStack(alignment: .leading, spacing: 10.0){
            Text("Current Condition :\(viewModel.description)")
                Text("Temperature :\(viewModel.temperature)")
            }
            RemoteImage(url: URL(string: viewModel.iconUrl)!, errorView: { error in
                Text(error.localizedDescription)
            }, imageView: { image in
                image
                .resizable()
                .frame(width: 100.0,height:100)
                .aspectRatio(contentMode: .fit)
            }, loadingView: {
                Text("Loading ...")
                })

      
           
                            
    }
    }
}

