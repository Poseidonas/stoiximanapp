//
//  CurrentViewModel.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 27/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import Foundation
import SwiftUI

struct CurrentViewModel: Identifiable {
    
    private let item: CurrentCondition


    var id = UUID()
    
    init(item: CurrentCondition) {
      self.item = item
    }
    
  
  
  var temp_C: String {
    return item.temp_C
  }
    var temperature:String{
        return item.temp_C  + "℃"
    }
    
    var description: String{
        return (item.weatherDesc[0]?.value)!
    }
    var iconUrl: String{
        return (item.weatherIconUrl[0].value)!
    }
  
  
 
  
 
}

