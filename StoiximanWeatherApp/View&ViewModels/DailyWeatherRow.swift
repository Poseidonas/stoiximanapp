//
//  DailyWeatherRow.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 27/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import SwiftUI

struct DailyWeatherRow: View {
  private let viewModel: DailyWeatherViewModel
  
  init(viewModel: DailyWeatherViewModel) {
    self.viewModel = viewModel
  }
  
  var body: some View {
   VStack(alignment: .leading){
     
    Text("\(self.viewModel.daydescription)").padding(.leading, 30).foregroundColor(.blue)
        
    ScrollView(.horizontal) {
        

    HStack(spacing: 10) {
       
        ForEach(self.viewModel.hourlysections, id:\.id) { hoursection in
            // HStack(spacing: 10) {
                VStack{
            
                Text("\(hoursection.timedescription)")
                Text("\(hoursection.temperature)")
                Text("\(hoursection.weatherDescribe)")
                    
                    RemoteImage(url: URL(string: hoursection.weatherIcon)!, errorView: { error in
                                   Text(error.localizedDescription)
                               }, imageView: { image in
                                   image
                                   .resizable()
                                   .frame(width: 40.0,height:40)
                                   .aspectRatio(contentMode: .fit)
                               }, loadingView: {
                                   Text("Loading ...")
                                   })
                    Spacer()

                    
                    

                
            

        }
        }}.frame(height: 120).padding(.leading, 20)
        .padding(.trailing, -20)
    }.id(UUID().uuidString)
    
    
        }
    }
}
