//
//  DailyWeatherViewModel.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 27/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import Foundation
import SwiftUI

struct DailyWeatherViewModel: Identifiable {
    
    private let item: WeatherStruct
    
    
    var id: String{
        return item.date + item.maxtempC + item.mintempC}
    
    init(item: WeatherStruct) {
        self.item = item
    }
    
    
    var daydescription: String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"                 // Note: S is fractional second
        let dateFromString = dateFormatter.date(from: day)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "MMM d, yyyy"
        
        let stringFromDate = dateFormatter2.string(from: dateFromString!)
        
        return  stringFromDate
        
        
        
        
    }
    
    
    
    var day: String {
        return item.date
    }
    var hourlysections : [HourlyStruct]{
        return item.hourly
    }
    
    
    var maxtemperature: String {
        return item.maxtempC
    }
    var mintemperature: String{
        return item.mintempC
    }
    
    
    
}

// Used to hash on just the day in order to produce a single view model for each
// day when there are multiple items per each day.
extension DailyWeatherViewModel: Hashable {
    static func == (lhs: DailyWeatherViewModel, rhs: DailyWeatherViewModel) -> Bool {
        return lhs.day == rhs.day
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.day)
    }
}
