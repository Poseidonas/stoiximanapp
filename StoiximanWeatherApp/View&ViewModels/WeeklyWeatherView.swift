//
//  WeeklyWeatherView.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 22/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import SwiftUI

struct WeeklyWeatherView: View {
  @ObservedObject var viewModel: WeeklyWeatherViewModel

  init(viewModel: WeeklyWeatherViewModel) {
    self.viewModel = viewModel
  }
  
  var body: some View {
    NavigationView {
      List {
        searchField

        if viewModel.dataSource.isEmpty {
          emptySection
        } else {
          querySection
          currentSection
          forecastSection
        }
      }
        .listStyle(GroupedListStyle())
        .navigationBarTitle("Weather ⛅️")
    }
  }
}

private extension WeeklyWeatherView {
  var searchField: some View {
    HStack(alignment: .center) {
      TextField("e.g. Cupertino", text: $viewModel.city)
    }
  }

  var forecastSection: some View {
    Section(header:
    Text("Hourly forecast for the next 7days").foregroundColor(.blue)) {
      ForEach(viewModel.dataSource, content: DailyWeatherRow.init(viewModel:))
    }
  }
    var currentSection: some View {
              Section(header:
              Text("Current condition").foregroundColor(.blue)){
                CurrentView(viewModel: viewModel.currentCondition!).frame(height:100.0)
              }
              
              
              
          }
    
    var querySection: some View{
        
        Section(header:
        Text("Results for the query").foregroundColor(.blue)){
            Text("\(viewModel.queryResult!.query)")
        }
    }
    
   
 

  var emptySection: some View {
    Section {
      Text("No results")
        .foregroundColor(.gray)
    }
  }
}
   


