//
//  WeeklyWeatherViewModel.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 27/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import SwiftUI
import Combine

class WeeklyWeatherViewModel: ObservableObject, Identifiable {
  @Published var city: String = ""
  @Published var dataSource: [DailyWeatherViewModel] = []
  @Published var currentCondition: CurrentViewModel? =  nil
  @Published var queryResult: Rstruct? = nil

  private let weatherFetcher: WeatherFetchable
  private var disposables = Set<AnyCancellable>()

  init(
    weatherFetcher: WeatherFetchable,
    scheduler: DispatchQueue = DispatchQueue(label: "WeatherViewModel")
  ) {
    self.weatherFetcher = weatherFetcher
    $city
      .dropFirst(1)
      .debounce(for: .seconds(0.5), scheduler: scheduler)
      .sink(receiveValue: fetchWeather(forCity:))
      .store(in: &disposables)
  }

  func fetchWeather(forCity city: String) {
    weatherFetcher.weeklyWeatherForecast(forCity: city)
        
        // 3
      
      .receive(on: DispatchQueue.main)
      .sink(
        receiveCompletion: { [weak self] value in
          guard let self = self else { return }
          switch value {
          case .failure:
            print("failure")
            self.dataSource = []
            self.queryResult = nil
            self.currentCondition = nil
          case .finished:
            break
          }
        },
        receiveValue: { [weak self] forecast in
          guard let self = self else { return }
            self.dataSource = forecast.data.weather.map{litem in
                DailyWeatherViewModel.init(item: litem)
            }
            self.currentCondition =
                CurrentViewModel.init(item: forecast.data.current_condition[0])
            self.queryResult = forecast.data.request[0]
             
            
           
          //  self.dataSource = forecast.data.weather.map{item in
          //      DailyWeatherRow.init(viewModel: item)}
           // print(forecast.description)
      })
      .store(in: &disposables)
  }
}
