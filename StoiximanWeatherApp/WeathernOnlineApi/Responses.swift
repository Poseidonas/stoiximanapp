/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation


struct WeekResponse: Decodable{
    
    let data : Dresponse
    
    enum CodingKeys: String, CodingKey {
        case data
        
    }
    
   
    
    
}

struct  Dresponse: Decodable {
       let request : [Rstruct]
       let current_condition : [CurrentCondition]
       let weather : [WeatherStruct]
    
    enum CodingKeys: String, CodingKey {
        case request
        case current_condition
        case weather
        
    }
    
   
   }
struct Rstruct : Decodable{
    
    let type : String
    let query: String
    enum CodingKeys: String, CodingKey {
           case type
           case query
           
       }
}

struct CurrentCondition: Decodable{
    let temp_C: String
    let temp_F: String
    let weatherDesc: [WeatherDesc?]
    let weatherIconUrl: [weatherIconUrl]
    enum CodingKeys: String, CodingKey {
             case temp_C
             case temp_F
             case weatherDesc
             case weatherIconUrl
             
         }
    
    
    
}
struct WeatherStruct: Decodable{
    
    let date : String
    let maxtempC: String
    let maxtempF: String
    let mintempC: String
    let hourly: [HourlyStruct]
    
    enum CodingKeys: String, CodingKey {
                case date
                case maxtempC
                case maxtempF
                case mintempC
                case hourly
                
            }
    
    
    
    
}
struct WeatherDesc: Decodable{
    let value : String?
    
     enum CodingKeys: String, CodingKey {
        case value
        
    }
}
struct weatherIconUrl: Decodable{
    let value : String?
       
        enum CodingKeys: String, CodingKey {
           case value
           
       }
    
}



struct HourlyStruct: Decodable, Identifiable{
   
    
    
    let time : String
    let tempC : String
    let weatherDesc : [WeatherDesc]
    let weatherIconUrl: [weatherIconUrl]

    var id: String = UUID().uuidString
    enum CodingKeys: String, CodingKey {
          case time
          case tempC
          case weatherDesc
          case weatherIconUrl
    }
    
    var weatherDescribe : String{
        return weatherDesc[0].value!
    }
    var weatherIcon: String{
        return weatherIconUrl[0].value!
    }
    var temperature : String{
        return tempC + "℃"
    }
    
    var timedescription : String {
        let timedescription = ["24":"09:00", "0":"24:00","300":"03:00","600":"06:00","900":"09:00","1200":"12:00","1500":"15:00","1800":"18:00","2100":"21:00"]
        return timedescription[time] ?? "00:00"
    }
    
    
}
   

