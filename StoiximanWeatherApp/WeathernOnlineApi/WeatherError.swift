//
//  WeatherError.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 27/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import Foundation

enum WeatherError: Error {
  case parsing(description: String)
  case network(description: String)
}
