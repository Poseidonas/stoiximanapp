//
//  WeatherFetcher.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 27/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import Foundation
import Combine

protocol WeatherFetchable {
  func weeklyWeatherForecast(
    forCity city: String
  ) -> AnyPublisher<WeekResponse, WeatherError>

 
}

class WeatherFetcher {
  private let session: URLSession
  
  init(session: URLSession = .shared) {
    self.session = session
  }
}

// MARK: - WeatherFetchable
extension WeatherFetcher: WeatherFetchable {
   
    
  func weeklyWeatherForecast(
    forCity city: String
  ) -> AnyPublisher<WeekResponse, WeatherError> {
    return forecast(with: makeWeeklyForecastComponents(withCity: city))
  }

 

  private func forecast(
    with components: URLComponents
  ) -> AnyPublisher<WeekResponse, WeatherError>  {
    guard let url = components.url else {
      let error = WeatherError.network(description: "Couldn't create URL")
      return Fail(error: error).eraseToAnyPublisher()
    }
    return session.dataTaskPublisher(for: URLRequest(url: url))
      .mapError { error in
        .network(description: error.localizedDescription)
        }.print()
      .flatMap(maxPublishers: .max(1)) { pair in
        
        decode(pair.data)
    }
      .eraseToAnyPublisher()
  }
}


// MARK: - OpenWeatherMap API
private extension WeatherFetcher {
  struct OpenWeatherAPI {
    static let scheme = "http"
    static let host = "api.worldweatheronline.com"
    static let path = "/premium/v1/weather.ashx"
    static let key = "3d96cec2e6db4504a8f204841201704"
  }
  
  func makeWeeklyForecastComponents(
    withCity city: String
  ) -> URLComponents {
    var components = URLComponents()
    components.scheme = OpenWeatherAPI.scheme
    components.host = OpenWeatherAPI.host
    components.path = OpenWeatherAPI.path
    
    components.queryItems = [
      URLQueryItem(name: "key", value: OpenWeatherAPI.key),
      URLQueryItem(name: "q", value: city),
      URLQueryItem(name: "format", value: "json"),
      URLQueryItem(name: "num_of_days", value: "7"),
      URLQueryItem(name: "fx24", value: "yes")]
    
    
    return components
  }
  
  func makeCurrentDayForecastComponents(
    withCity city: String
  ) -> URLComponents {
    var components = URLComponents()
    components.scheme = OpenWeatherAPI.scheme
    components.host = OpenWeatherAPI.host
    components.path = OpenWeatherAPI.path + "/weather.ashx"
    
    components.queryItems = [
      URLQueryItem(name: "q", value: city),
      URLQueryItem(name: "mode", value: "json"),
      URLQueryItem(name: "units", value: "metric"),
      URLQueryItem(name: "key", value: OpenWeatherAPI.key)
    ]
    
    return components
  }
}

