//
//  Parsing.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 20/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import Foundation
import Combine

func decode<T: Decodable>(_ data: Data) -> AnyPublisher<T, WeatherError> {
    print("inside decode")
    
    
    let decoder = JSONDecoder()
    
    return Just(data)
        .decode(type: T.self, decoder: decoder)
        .mapError { error in
            .parsing(description: error.localizedDescription)
    }
    .eraseToAnyPublisher()
}


func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
    
    let inputFormatter = DateFormatter()
    inputFormatter.dateFormat = "dd/MM/yyyy"
    
    if let date = inputFormatter.date(from: dateString) {
        
        let outputFormatter = DateFormatter()
        outputFormatter.dateFormat = format
        
        return outputFormatter.string(from: date)
    }
    
    return nil
}

