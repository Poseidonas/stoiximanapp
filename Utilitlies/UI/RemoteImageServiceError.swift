//
//  RemoteImageServiceError.swift
//  StoiximanWeatherApp
//
//  Created by Charilaos Laliotis on 27/4/20.
//  Copyright © 2020 Charilaos Laliotis. All rights reserved.
//

import Foundation


enum RemoteImageServiceError: Error {
    case couldNotCreateImage
}

extension RemoteImageServiceError: LocalizedError {
    var errorDescription: String? {
        return "Could not create image from received data"
    }
}
